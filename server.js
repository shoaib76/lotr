const express = require('express')
const morgan = require('morgan')


const app = express()

app.use(morgan(":method :url :status :res[content-length] - :response-time ms"))

// https://gist.githubusercontent.com/meech-ward/1723b2df87eae8bb6382828fba649d64/raw/ee52637cc953df669d95bb4ab68ac2ad1a96cd9f/lotr.sql





app.get("/test", (req, res) => {
  res.send("<h1>It's working 🤗</h1>")
})





const port = process.env.PORT || 8080
app.listen(port, () => console.log(`Listening on port ${port}`))
